This is a test automation framework based on BDD Cucumber written in Ruby.

Front-end automation and Back-end automation use required Ruby gems listed in Gemfile.
 
Sample features and steps have been set up. Any future smoke and functional test automation can be built within this framework and expand.

Automated build/test can be set up with Jenkins if needed.