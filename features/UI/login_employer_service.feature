@smoke @UI
Feature: employers login employer service CBX ATS

  Scenario Outline: employers login employer service CBX ATS
    When employers provides "<email>" and "<password>"to login ATS
    Then employers successfully login ATS service or not
    
    Examples: list of emails and passwords
    		  |email                    |password |
    		  |123@abcd.com             |password |
    		  |||
    		  |                         |         |
    		  |1a@$#&?>99999999999      |?????!!!!|
    
    
    