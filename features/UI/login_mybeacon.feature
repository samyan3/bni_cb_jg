@smoke @UI
Feature: job seekers visit careerbeacon and login mybeacon

  Scenario Outline: job seekers visit careerbeacon and login mybeacon
    Given job seekers go to careerbeacon.com and click mybeacon
    When job seekers provide "<email>" and "<password>", clicks login button
    Then job seekers login mybeacon or not
    
    Examples: list of emails and passwords
    		  |email              |password  |
    		  |||
    		  |abc@none.com       |abc123    |
    		  |                   |          |
    		  |1a@$#&?>99999999999|?????!!!!!|
    
  
    
    
    
    