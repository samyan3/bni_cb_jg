@smoke @UI
Feature: resize browser window to simulate mobile site

  Background:
  	Given open a browser, go to careerbeacon.com
  
  Scenario: resize browser window to simulate mobile careerbeacon site
    
    When resize browser window to simulate mobile site
    Then it displays as mobile site displays
    
  Scenario Outline: a mobile user search jobs using keywords
    
    When mobile user input a "<keyword>" to search
    Then it returns job results
    
    Examples: list of keywords to search
    		  |keyword     |
    		  |test        |
    		  |SMM020216-CB|
    		  |MB1604131665|
    		  |            |
    