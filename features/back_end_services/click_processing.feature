@service
Feature: check click processing service log after start daily
		 ensure it is started without failure

  
  Scenario Outline: check click processing service log after start daily
    Given SSH connected to the stage "<stagenumber>"
    When check its service log named "<service>" at path "<path>" for "<trace>"
    Then its logs shows this trace
    
    Examples: provides host IP and service log name
    		|stagenumber|service         |path                  |trace                   |
    		|1          |Click_Processing|/var/log/careerbeacon |Click Processing Started|
    		|2          |Click_Processing|/var/log/careerbeacon |Click Processing Started|
    		|3          |Click_Processing|/var/log/careerbeacon |Click Processing Started|
    		|5          |Click_Processing|/var/log/careerbeacon |Click Processing Started|
    		
  Scenario Outline: check click processing service log
    Given SSH connected to the stage "<stagenumber>"
    When check its service log named "<service>" at path "<path>" for "<trace>"
    Then its logs should not show this trace
    
    Examples: host IP and service log name
    		|stagenumber|service         |path                  |trace                   |
    		|1          |Click_Processing|/var/log/careerbeacon |Connection refused      |
    		|2          |Click_Processing|/var/log/careerbeacon |Connection refused      |
    		|3          |Click_Processing|/var/log/careerbeacon |Connection refused      |
    		|5          |Click_Processing|/var/log/careerbeacon |Connection refused      |
    		
    		