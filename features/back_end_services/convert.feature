@service
Feature: check convert log

  Scenario Outline: check convert log daily
    Given SSH connected to the stage "<stagenumber>"
    When check its service log named "<service>" at path "<path>" for "<trace>"
    Then its logs should not show this trace 
    
    Examples: provides host IP and service log name
    		|stagenumber|service    |path                  |trace          |
    		|3          |convert-log|/var/log/careerbeacon |Failed to queue|
    		