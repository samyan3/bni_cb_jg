@database
Feature: show all tables from schema mybeacon

  Scenario: list all tables from schema mybeacon on stage
    Given SQL client connected to the host where MySQL server running
    When login user root and run the query
    Then it lists all available tables