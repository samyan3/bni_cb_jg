@database
Feature: check Mysql db table for job posting categories id using job posting id

  Scenario Outline: check Mysql db table for job posting categories id using job posting id
    Given connected to the stage "<stagenumber>" where SQL server is running
    When I provides the job posting id "<postingid>" to search the db table "<dbtable>"
    Then all its categories id are returned
       
    Examples: check categories in db
    		|stagenumber|postingid|dbtable                 |
    		|3          |521889   |mbpostingcategoryposting|
    		|3          |521757   |mbpostingcategoryposting|
    		|3          |999999   |mbpostingcategoryposting|
    		|1          |521757   |mbpostingcategoryposting|
    		|2          |521757   |mbpostingcategoryposting|
    		|5          |521757   |mbpostingcategoryposting|
    		
    		