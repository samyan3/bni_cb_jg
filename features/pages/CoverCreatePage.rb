require 'page-object'
class CoverCreatePage
    include PageObject
    #page_url('https://stage2-www.careerbeacon.com/Login')
    page_url('https://www.careerbeacon.com/document/view/cover-letters#Create')
    #text_field(:email, id: 'username')
    #text_field(:password, id: 'password')
    link(:create_cover, partial_link_text: 'editor')
    link(:upload_doc, partial_link_text: 'document')
    text_field(:cover_title, name: 'title')
    
    file_field(:locate_file, id: 'choose-file-real') # works when <input type="file">
    button(:upload, id: 'upload-form-button')
    
    
    def edit_cover_letter
     
      self.create_cover;
      
    end
    
    
    def upload_cover_letter
     
      self.upload_doc;
      
    end
    
    def edit_title(title) # two same elements of cover letter title text field, switch tabs
      
      #print self.cover_title_element;
      self.cover_title=title;
      
    end
    
    def set_title(title) # two same elements of cover letter title text field, switch tabs
      
      #self.editor_title_element.clear;
      #self.editor_title_element.click
      self.editor_title=title;
      
    end
    
    def select_file(file)
      
     
      self.locate_file=file; sleep 1
      
    end
    
    def upload_file
      
      self.upload; sleep 5
      
    end
    
    
    
end