require 'page-object'
class CoverLetter
    include PageObject
    #page_url('https://stage2-www.careerbeacon.com/Login')
    page_url('https://www.careerbeacon.com/document/view/cover-letters')
    #text_field(:email, id: 'username')
    #text_field(:password, id: 'password')
    link(:create_cover, partial_link_text: 'editor')
    text_field(:editor_title, css: '#document-create > form > div.input-field > div > input[type="text"]')
   
    #text_area(:rich_text, css: '.cke_wysiwyg_frame.cke_reset')
    in_iframe(:css => '#cke_1_contents > iframe') do |frame|
        text_field(:rich_text, :frame => frame)
    end
    
   
    link(:delete, partial_link_text: 'Delete')
    button(:yes, id: 'delete-document-button')
    
    button(:saveit, css: "#document-create > form > p:nth-child(4) > button")
    
   
    def delete_cover
      
      self.delete; sleep 1
      
    end
    
    def delete_yes
     
      self.yes;
      
    end
    
    def edit_cover_letter
     
      self.create_cover;
      
    end
    
    
    def set_title(title) 
      #puts editor_title_element
      #self.editor_title_element.click
      #self.editor_title_element.clear;
      self.editor_title=title;
      
    end
    
    def set_text(text) 
      #puts self.rich_text_element
      self.rich_text_element.clear;
      self.rich_text_element.click
      #self.editor_title_element.clear;
      self.rich_text=text;
      
    end
    
    def click_save
      puts 'haha'
      self.saveit;
      
    end
    
    
end