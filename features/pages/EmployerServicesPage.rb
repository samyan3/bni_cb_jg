require 'page-object'
class EmployerServicesPage
    include PageObject
    #page_url('http://stage2-www.careerbeacon.com')
    page_url('https://www.careerbeacon.com/EmployerServices')
    button(:employer_login_button, class: 'employer_login_button')
    
    
    def login
      self.employer_login_button
      
    end
    
   
end