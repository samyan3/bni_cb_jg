require 'page-object'
class HomePage
    include PageObject
    #page_url('http://stage2-www.careerbeacon.com')
    page_url('http://www.careerbeacon.com')
    link(:mybeacon, partial_link_text: 'MyBeacon')
    link(:login, partial_link_text: 'Log In')
    link(:employerprofile, partial_link_text: 'Employer Profiles')
    link(:employerservice, partial_link_text: 'Employer Services')
    text_field(:query, id: 'search-query')
    button(:search, id: 'search-btn')
    
    
    def click_mybeacon
      self.mybeacon
      
    end
    
    def click_login
      self.login
      
    end
    
    def click_employer_profile
      self.employerprofile
      
    end
    
    def click_employer_service
      self.employerservice
      
    end
    
    def input_keyword(keyword)
      self.query=keyword
      
    end
    
    def click_search
      self.search
      
    end
end