require 'page-object'
class JobSeekerAccountPage
    include PageObject
    #page_url('https://stage2-www.careerbeacon.com/Login')
    page_url('https://www.careerbeacon.com/account')
    #text_field(:email, id: 'username')
    #text_field(:password, id: 'password')
    link(:new_cover, partial_link_text: 'Create a new Cover Letter')
    
    def new_cover_letter
      self.new_cover; sleep 1
    end
end