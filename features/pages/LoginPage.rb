require 'page-object'
class LoginPage
    include PageObject
    #page_url('https://stage2-www.careerbeacon.com/Login')
    page_url('https://www.careerbeacon.com/Login')
    text_field(:email, id: 'username')
    text_field(:password, id: 'password')
    button(:login, id: 'submit-login')
    
    def login_mybeacon(email,pw)
      self.email = email
      self.password = pw; sleep 1
      self.login; 
      
    end
end