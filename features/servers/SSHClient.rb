class SSHClient
  @@number_ssh_clients=0
  
  def initialize(host,user,port)
    @ssh_host=host
    @ssh_user=user
    @ssh_port=port
    
    @@number_ssh_clients +=1
    
  end
  
  
  def start_ssh_client
    #running Pagent where private key loaded in system tray 
    @ssh = Net::SSH.start(@ssh_host, @ssh_user, :port => @ssh_port)
    #puts 'SSH Client started!'
    
  end
  
  def run_cmds(cmd)
    stdout = ""
   
    @ssh.exec!(cmd) do |channel, stream, data|
        #stdout << data if stream == :stdout
        stdout << data
     
    end
    stdout #return it

    
  end
  
  def ssh_clients_count
    @@number_ssh_clients
  end
  
  
  def ssh_clients_close
    @ssh.close
  end
end

