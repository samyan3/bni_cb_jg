
Given(/^job seeker login mybeacon$/) do
    ENV['HTTP_PROXY'] = ENV['http_proxy'] = nil
    
    
    @driver = Selenium::WebDriver.for :chrome
    @driver.manage().window().maximize();
    
    home_page = HomePage.new(@driver,true); 
    #home_page.goto; sleep 1
    home_page.click_mybeacon
    
    login_page = LoginPage.new(@driver)
    usr=''
    pw=''
    login_page.login_mybeacon(usr, pw); sleep 3
    
end

When(/^job seeker create a cover letter using upload or editor$/) do
    account_page = JobSeekerAccountPage.new(@driver)
    account_page.new_cover_letter; sleep 3
    
    create_page = CoverCreatePage.new(@driver)
    
    #@driver.manage.window().resize_to(1000, 800);
   
    #create_page.edit_cover_letter; sleep 3 # two same elements of cover letter title text field, switch tabs
    
    create_page.upload_cover_letter; sleep 1 # two same elements of cover letter title text field, switch tabs 
    
    create_page.edit_title('cover_letter_1')
    file='path to TestResume1.docx'
    #file='C:\fakepath\TestResume1.docx'
    create_page.select_file(file)
    create_page.upload_file
    
    cover_letters = CoverLetter.new(@driver)
    cover_letters.delete_cover
    cover_letters.delete_yes
    
   
    cover_letters.edit_cover_letter; sleep 5
    
    cover_letters.set_title('cover_letter_2')
    #cover_letters.set_text('edited cover letter')
    
    # use driver directly to locate iframe, then switch into it
    ckeditor_frame = @driver.find_element(:css => '#cke_1_contents > iframe')
    @driver.switch_to.frame(ckeditor_frame)
   
    editor_body = @driver.find_element(:tag_name => 'body')
    editor_body.send_keys("please ignore this cover letter.")
    
    # ensure switch out of iframe
    @driver.switch_to.default_content
    
    cover_letters.click_save
    
end

Then(/^cover letter successfully saved and then deleted$/) do
  
  puts 'cover letter successfully saved, then deleted!'
  #sleep 2; @driver.quit
end




