require "selenium-webdriver"

Given(/^job seekers go to careerbeacon\.com and click mybeacon$/) do
  puts 'job seekers go to careerbeacon.com and click mybeacon'
end

When(/^job seekers provide "([^"]*)" and "([^"]*)", clicks login button$/) do |arg1, arg2|
    ENV['HTTP_PROXY'] = ENV['http_proxy'] = nil
    
    driver = Selenium::WebDriver.for :chrome
    driver.manage().window().maximize();
    
    home_page = HomePage.new(driver,true); 
    #home_page.goto; sleep 1
    home_page.click_mybeacon
    
    login_page = LoginPage.new(driver)
    login_page.login_mybeacon(arg1, arg2); sleep 3
    
    
    driver.quit

end

Then(/^job seekers login mybeacon or not$/) do
  puts 'job seekers login mybeacon or not'
end