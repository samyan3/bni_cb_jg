require "selenium-webdriver"

Given(/^open a browser, go to careerbeacon\.com$/) do
    ENV['HTTP_PROXY'] = ENV['http_proxy'] = nil
    
    @driver = Selenium::WebDriver.for :chrome
    @driver.manage().window().maximize();
    
    @home_page = HomePage.new(@driver,true); 
end

When(/^resize browser window to simulate mobile site$/) do
    @driver.manage.window().resize_to(500, 700); #mobile size
end

Then(/^it displays as mobile site displays$/) do
    puts 'start simulating mobile site!'
    sleep 2; @driver.quit
end


When(/^mobile user input a "([^"]*)" to search$/) do |arg1|
  @driver.manage.window().resize_to(500, 700); #mobile size
  @home_page.input_keyword(arg1)
  @home_page.click_search; sleep 3
  
  close = @driver.find_element(:id, 'favsearch-modal-close') # no need of a page for this
  close.click
  
end

Then(/^it returns job results$/) do
  puts 'please review returned job results'
  sleep 3; @driver.quit
end
