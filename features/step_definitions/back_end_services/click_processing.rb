require 'net/ssh'


Given(/^SSH connected to the stage "([^"]*)"$/) do |arg1|

  #puts $stage1_batch_ip
  case arg1
  when '1'
    
    arg1 = $stage1_batch_ip
    puts 'this is stage 1 batch IP', arg1
  when '2'
  
    arg1 = $stage2_batch_ip
    puts 'this is stage 2 batch IP', arg1
  when '3'
   
    arg1 = $stage3_batch_ip
    puts 'this is stage 3 batch IP', arg1
  when '5'
    
    arg1 = $stage5_batch_ip
    puts 'this is stage 5 batch IP', arg1
  end
  
  
  @sshclient=SSHClient.new(arg1,'syan',22)
  @sshclient.start_ssh_client
  
  #print 'number of SSH clients created: ' , @sshclient.ssh_clients_count, "\n"
  
end

When(/^check its service log named "([^"]*)" at path "([^"]*)" for "([^"]*)"$/) do |arg1, arg2, arg3|
  
  time =Time.new
  date=time.strftime("%Y-%m-%d")
  
  if arg1.include? '-log'
      date2=time.strftime("%Y%m%d")
      @cmd = "cd " + arg2 + " && " + "grep -i '" + arg3 + "' " + arg1 + "_" + date2 + ".log" + "|grep " + date + "|tail -n10 "
   
  else
      @cmd = "cd " + arg2 + " && " + "grep -i '" + arg3 + "' " + arg1 + ".log" + "|grep " + date
  end
  #puts @cmd
  puts "\n"
  
  
end

Then(/^its logs shows this trace$/) do
  #puts "\n", '########################################################################', "\n"
  stdout = @sshclient.run_cmds(@cmd)
  
  if stdout.empty?
    puts 'found no matching line.', "\n"
     
    fail "Failed! I don't expect this to be null!" 
  else
    
    puts 'Success! found matching line.', "\n"
    puts stdout
  end
  
  @sshclient.ssh_clients_close
end

Then(/^its logs should not show this trace$/) do
  stdout = @sshclient.run_cmds(@cmd)
  
  if stdout.empty?
    puts 'Success! found no this trace', "\n"
     
  else
    
    puts 'found this trace', "\n"
    puts stdout
    fail "Failed! I expect this to be null!"
  end
  @sshclient.ssh_clients_close
end

