class MySQLClient
  @@number_mysql_client=0
  
  def initialize(host,user,db,port)
    @client_host=host
    @client_user=user
    @client_db=db
    @client_port=port
    
    @@number_mysql_client +=1
  end
  
  def open_gateway_ports
    #using net ssh gateway tunnel, and running Pagent where private key loaded in system tray
    gateway = Net::SSH::Gateway.new(
      @client_host,
      @client_user
     )
  
    @ports = gateway.open('127.0.0.1', @client_port)
    #puts 'gateway ports opened!'
    
  end
  
  
  def start_client
    
    @myclient = Mysql2::Client.new(
    host: "localhost",
    username: '',
    password: '',
    database: @client_db,
    port: @ports,
    #secure_auth: 'false'
    )
    #puts 'sql client started' 
  end
  
  def run_query(query)
    
    query.each do |cmd|
      results = @myclient.query(cmd)
      results.each do |row|
          puts row
      end
    end
    @myclient.close
  end
  
  def run_cmd(cmd)
    
    @myclient.query(cmd)
      
    #@myclient.close
  end
  
  def sql_clients_count
    @@number_mysql_client
  end
  
  
  def close_client
    @myclient.close
  end
end