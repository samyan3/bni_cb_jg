require 'net/ssh'
require 'mysql2'
require 'net/ssh/gateway'

Given(/^SQL client connected to the host where MySQL server running$/) do

  @sqlclient = MySQLClient.new('54.89.28.255','syan','mybeacon',3306)
  @sqlclient.open_gateway_ports
  print 'number of Mysql clients created: ' , @sqlclient.sql_clients_count, "\n"
  
  
end

When(/^login user root and run the query$/) do
  
  @sqlclient.start_client

end

Then(/^it lists all available tables$/) do
 
  query = ["show tables;","SELECT FOUND_ROWS()"]
  @sqlclient.run_query(query)
  
  puts 'all tables are displayed!'
 
end 