require 'net/ssh'
require 'net/ssh/gateway'
require 'mysql2'

Given(/^connected to the stage "([^"]*)" where SQL server is running$/) do |arg1|
  
  case arg1
  when '1'
    
    arg1 = $stage1_database_ip
    puts 'this is stage 1 database IP', arg1
  when '2'
  
    arg1 = $stage2_database_ip
    puts 'this is stage 2 database IP', arg1
  when '3'
   
    arg1 = $stage3_database_ip
    puts 'this is stage 3 database IP', arg1
  when '5'
    
    arg1 = $stage5_database_ip
    puts 'this is stage 5 database IP', arg1
  end
  
  @sqlclient=MySQLClient.new(arg1,'syan','mybeacon',3306)
  @sqlclient.open_gateway_ports
end

When(/^I provides the job posting id "([^"]*)" to search the db table "([^"]*)"$/) do |arg1, arg2|

  @postingid=arg1
  @dbtable=arg2
  @sqlclient.start_client
end

Then(/^all its categories id are returned$/) do
  
  #query = ["show tables;","SELECT FOUND_ROWS()"]
  cmd = "SELECT * FROM " + @dbtable + " where mbpostingid=" + @postingid + ";"
  results = @sqlclient.run_cmd(cmd)
  
  if results.count == 0
    puts "\n",'found no matching records!'
    fail 'check if this is correct posting id or if posting associated with categories '
  else
    results.each do |row|
        puts "\n",row
    end
    
  end
end