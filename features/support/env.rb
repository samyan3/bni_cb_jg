require 'yaml'

# Config file to hold all VM IPs
#CONFIG = YAML::load_file(File.join(File.dirname(File.expand_path(__FILE__)), 'config.yml'))
CONFIG = YAML.load_file("features/support/config.yml")
$stage1_batch_ip = CONFIG['stage1']['batch']['ip']
$stage1_database_ip = CONFIG['stage1']['database']['ip']

$stage2_batch_ip = CONFIG['stage2']['batch']['ip']
$stage2_database_ip = CONFIG['stage2']['database']['ip']

$stage3_batch_ip = CONFIG['stage3']['batch']['ip']
$stage3_database_ip = CONFIG['stage3']['database']['ip']

$stage5_batch_ip = CONFIG['stage5']['batch']['ip']
$stage5_database_ip = CONFIG['stage5']['database']['ip']


